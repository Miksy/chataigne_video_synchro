from medusa_mpv import MedusaMPV
from medusa_osc import MedusaOSC


def main():
    player = MedusaMPV()
    myOsc = MedusaOSC()
    player.load("Genos_H265_5.1.mp4")

    # def emit_end(data):
    #     myOsc.send_message(b'/vlc/end', [0], '127.0.0.1', port=3750)
    #     player.reload()

    @myOsc.osc.address(b'/chataigne/play')
    def play(*values):
        # time arrives from chataigne as a float in seconds -> 15.24
        # conveniently MPV takes time-pos in seconds
        curr_time = values[0]
        player.goTo(curr_time)
        player.play()

    @myOsc.osc.address(b'/chataigne/pause')
    def pause(*values):
        player.pause()

    @myOsc.osc.address(b'/chataigne/seek')
    def seek(*values):
        curr_time = values[0]
        player.goTo(curr_time)
    
    input("""
=====================
||  COUCOU LE 104  ||
=====================
 
Ce script lance un lecteur video et le logiciel Chataigne
Pour l'arreter, il faut fermer Chataigne
Ensuite, cliquer sur cette fenetre et appuyer sur la touche ENTREE
 
=====================
||  COUCOU LE 104  ||
=====================
    """)
    player.pause()
    myOsc.kill()


if __name__ == "__main__":
    main()