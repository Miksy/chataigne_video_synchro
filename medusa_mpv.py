# fix for "mpv-2.dll not in PATH"
import os
os.environ["PATH"] = os.path.dirname(__file__) + os.pathsep + os.environ["PATH"]
import mpv
from time import sleep

class MedusaMPV():

    def __init__(self):
        self.player = mpv.MPV(osc=True)
        self.player.keep_open = 'yes'

        # make video fullscreen on 2nd monitor
        self.player.fs_screen = 1
        self.player.fullscreen = True

    def load(self, source):
        self.player.loadfile(source)
        self.pause()

        # keep fullscreen on 2nd monitor but user can change it with UI
        sleep(1)
        self.player.fs_screen = 'current'

    def play(self):
        self.player.pause = False

    def pause(self):
        self.player.pause = True

    def stop(self):
        self.player.stop()

    def goTo(self, val):
        self.player.time_pos = val


def main():
    player = MedusaMPV()
    player.load('TDMovieOut.0.mov')

    input('press ENTER to exit')


if __name__ == "__main__":
    main()