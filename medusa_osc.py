from time import sleep
from oscpy.server import OSCThreadServer

class MedusaOSC() :
    
    def __init__(self):
        self.osc = OSCThreadServer()
        self.sock = self.osc.listen(address='127.0.0.1', port=3751, default=True)

    def send_message(self, name, values, ip, port):
        self.osc.send_message(name, values, ip, port)

    def stop(self):
        self.osc.stop() # Stop the default socket
        self.osc.stop_all() # Stop all sockets

    def terminate(self):
        self.osc.terminate_server()  # Request the handler thread to stop looping
        # Here the server is still alive, one might call osc.listen() again
        self.osc.join_server()  # Wait for the handler thread to finish pending tasks and exit
    
    def kill(self):
        self.stop()
        self.terminate()