# chataigne_video_synchro

I made this tool to synchronize video with DMX triggers.
This script synchronizes Chataigne's timeline with a video played by MPV video player.

## Installation

This repository depends on these languages/softwares, you have to install them :

-   [Chataigne](https://benjamin.kuperberg.fr/chataigne/)
-   [mpv player](https://github.com/mpv-player/mpv)
-   python 3.x
-   python-mpv package
-   oscpy package

MPV player may be difficult to install, I provided the .dll file and a hack in the python code to put it in the PATH.
