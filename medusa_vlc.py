# importing time and vlc
import vlc
from time import sleep

class MedusaVLC():

    def __init__(self):
        self.vlc_instance = vlc.Instance()
        self.player = self.vlc_instance.media_player_new()
        self.events = self.player.event_manager()
        # self.events.event_attach(vlc.EventType.MediaPlayerEndReached, self.vlcOnEndReached)
        self.loop = False
        self.duration = 0
        # self.events.event_attach(vlc.EventType.MediaPlayerPositionChanged, self.onPositionChanged)
        # self.events.event_attach(vlc.EventType.MediaPlayerPaused, self.onPause)
        # self.events.event_attach(vlc.EventType.MediaPlayerStopped, self.onStop)

    # def onPause(self, data):
    #     print('onPause')
    
    # def onStop(self, data):
    #     print('onStop')
    def onMediaPlayerEndReached(self, callback):
        self.events.event_attach(vlc.EventType.MediaPlayerEndReached, callback)

    def load(self, source):
        # creating a media
        self.media = self.vlc_instance.media_new(source)
        # setting media to the player
        self.player.set_media(self.media)
        self.duration = self.player.get_length()

        self.play()
        sleep(0.1)
        self.pause()

    def play(self):
        self.player.play()

    def pause(self):
        self.player.pause()

    def stop(self):
        self.player.stop()

    def get_time(self):
        return self.player.get_time()

    def goTo(self, val):
        self.duration = self.player.get_length()
        if(val > self.duration):
            val = self.duration
        self.player.set_time(val)